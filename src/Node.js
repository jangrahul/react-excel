import React, { Component } from 'react'
import HotTable from 'react-handsontable'


export default class Node extends Component{
   render() {
    return (
      <div className='node'>
        <div className='node_inner'>
          <h3 style={{textAlign: "center"}}>{this.props.text}</h3>
          {
            (this.props.data.length!==0) &&
            <HotTable root="node" ref="node" settings={{
            data: this.props.data,
            colHeaders: this.props.headers,
            rowHeaders: true,
            cells: this.props.cellProperties,
            width: "700",
            height: "200",
            onAfterChange: this.props.onAfterChange,
            contextMenu: ["remove_row"],
            onAfterRemoveRow: this.props.onAfterRemoveNode,
            onAfterCreateRow: this.props.onAfterCreateNode,
            onBeforeCreateRow: this.props.onBeforeCreateNode,
            // onAfterRender: selectFirst.bind(this)
            }}/>
          }
          {
            this.props.data.length===0 && <p>No Nodes</p>
          }
        <button onClick={this.props.closePopup}>Done</button>
        { (this.props.text!=="bts_bbu") &&
          <button onClick={this.props.addRow}>Add Row</button>}
        </div>
      </div>
    );
  }
}

function selectFirst(){
  const instance=this.refs.main.hotInstance
  instance.selectCell(1,1)

}
