import React, { Component } from 'react'
import HotTable from 'react-handsontable'
import Handsontable from 'handsontable'
import Config from './Config'
import Input from './Input'
import Excel from './Excel'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state={config:Config(),
                input:Input(),  // The Input JSON string
                cells:[],       // cells defines type,enabled properties of each cell and, the list of keys to be parsed from input
                rows:[],        // The parsed input JSON (part of final Data source for HotTable)
                nodeHeaders:[],
                nodeRows:[],
                nodeInput:[],
                showNodes: [-1,-1,""],
                showTab: "RF"}
    let cells=this.state.config.cells
    this.is_2g=cells[2].default
    this.is_3g=cells[3].default
    this.is_4g_td=cells[4].default
    this.is_4g_fd=cells[5].default
    this.is_4g=this.is_4g_fd||this.is_4g_td
    headersAndData=headersAndData.bind(this)
    addRows=addRows.bind(this)
    setShowNodes=setShowNodes.bind(this)
    showOtherNodes=showOtherNodes.bind(this)
    addNodeRow=addNodeRow.bind(this)
    parseInput=parseInput.bind(this)
    excel=excel.bind(this)
    handleTabSwitch=handleTabSwitch.bind(this)
    updateTab=updateTab.bind(this)
    scanNodes=scanNodes.bind(this)
    handleNodeChange=handleNodeChange.bind(this)
  }

  render() {
    
    return (
      <Tabs onSelect={handleTabSwitch}>
          <TabList>
            <Tab>RF</Tab>
            <Tab>MW</Tab>
            <Tab>Fiber</Tab>
          </TabList>

          <TabPanel>
            {this.state.showTab==="RF" && excel()}
          </TabPanel>
          <TabPanel>
            {this.state.showTab==="MW" && excel()}
          </TabPanel>
          <TabPanel>
            {this.state.showTab==="fiber" && excel()}
          </TabPanel>         
        </Tabs>
    );
  }
}



function excel(){
  var row, col, keys, nodeText, nodes, value;
  parseInput()
  if(this.state.showNodes[0]!==-1){
      [row,col,keys]=this.state.showNodes
      nodeText =keys.split('.').pop()
      nodes=this.state.config[nodeText]
      value = getValue(this.state.input[row],keys.split("."))
    }
  return(
     <Excel 
      config={this.state.config}
      input={this.state.input}
      cells={this.state.cells}
      rows={this.state.rows}
      values={this.data}
      mergeCells={this.mergeCells}
      addRows={addRows}
      showNodes={this.state.showNodes}
      nodeText={nodeText}
      setShowNodes={setShowNodes}
      addNodeRow={addNodeRow}
      nodeHeaders={this.state.nodeHeaders}
      nodeRows={this.state.nodeRows}
      nodeInput={this.state.nodeInput}
      keys={keys}
      nodes={nodes}
      value={value}
      parseInput={parseInput}
      showOtherNodes={showOtherNodes}
      updateTab={updateTab}
      handleNodeChange={handleNodeChange}
    />)
}

function handleTabSwitch(index,last){
  var tab
  switch(index){
    case 0:
      tab="RF"
      break;
    case 1:
      tab="MW"
      break;
    case 2:
      tab="fiber"
      break;
    default:
      tab="RF"
  }
  this.setState({
    showTab: tab
  })
}


//forms final Data Source for HotTable by combining header cells and this.state.rows
function headersAndData(){
    const config= this.state.config
    var headers_1 
    var headers_2
    let data=[]
    let headers=[]
    let mergeCells = []
    
    switch(this.state.showTab){
      case "RF":
        headers_1 = config.h1.concat(config.rf_h1)
        headers_2= config.h2.concat(config.rf_h2) 
        break;
      case "MW":
        headers_1 = config.h1.concat(config.mw_h1)
        headers_2= config.h2.concat(config.mw_h2)
        break;
      case "fiber":
        headers_1 = config.h1.concat(config.fiber_h1)
        headers_2= config.h2.concat(config.fiber_h2)
        break;
      default:
        headers_1 = config.h1.concat(config.rf_h1)
        headers_2= config.h2.concat(config.rf_h2) 
    }

    headers_1.forEach((h1,i)=>{
      headers=headers.concat(Array(h1.offset-headers.length).fill(""))
      if(h1.span!==0){
        mergeCells.push({row: 0, col: headers.length, rowspan: 1, colspan: h1.span})
      headers.push(h1.label)
      headers=headers.concat(Array(h1.span-1).fill(""))
      }
    })

    let len_diff = headers_2.length-headers.length
    headers=headers.concat(Array(len_diff).fill(""))

    data.push(headers,headers_2)
    data=data.concat(this.state.rows)
    this.data=data                    // final data source for HotTable
    this.mergeCells=mergeCells 
}


//sets this.state.cells w.r.t current active tab and parses input JSON
function parseInput(){
  const config = this.state.config
  switch(this.state.showTab){
    case "RF":
      this.state.cells=config.cells.concat(config.rf_cells)
      break;
    case "MW":
      this.state.cells=config.cells.concat(config.mw_cells)
      break;
    case "fiber":
      this.state.cells=config.cells.concat(config.fiber_cells)
      break;
    default:
      this.state.cells=config.cells.concat(config.rf_cells)
  }
  let rows=[]
  var cells = this.state.cells
  this.state.input.forEach(input=>{
    let row=[]
    cells.forEach(cell=>{
      if(cell.key){
        var value=getValue(input,cell.key.split('.'))
        if(cell.type==="dropdown" && value!==null){
          var option=cell.options.filter(opt=> opt.value===value)
          if(option[0]!==undefined)
            value=option[0].label
        }
        row.push(value)
      }
    })
    rows.push(row)
  })
  this.state.rows=rows
  headersAndData()
}


function addRows(event,count=0){
  if(event.preventDefault){
    event.preventDefault();
    count=parseInt(event.target.count.value)
  }
  
  const config=this.state.config
  var cells = config.cells.concat(config.rf_cells, config.mw_cells, config.fiber_cells)
  
  for(var i=0;i<count;i++){
    let row={}

    cells.forEach(cell=>{
      var value=cell.default
      if(Array.isArray(value))
        value=[]
      addValue(row,cell.key.split("."),value)
    })

    this.state.input.push(row)
  }
  parseInput()
  this.forceUpdate()
}

function showOtherNodes(row, col, values){
  let key = this.state.cells[col].key
  this.state.nodeInput=values
  this.state.nodeHeaders=this.state.config[key.split('.').pop()+"_headers"]
  let sub_key = key.split('.').pop()

  this.state.nodeRows=scanNodes(values,this.state.config[sub_key])
  setShowNodes(row-2,col,key)
}

function setShowNodes(row,col,key){
  this.setState({
    showNodes: [row,col,key]
  })
}

function addNodeRow(row, keys){
  var def_row={}
  var nodes = this.state.input[row]
  keys=keys.split(".")
  keys.forEach(key=>{
    nodes=nodes[key]
  })
  var cells = this.state.config[keys[1]]
  
  cells.forEach(cell=>{
    def_row[cell.key]=cell.default
  })
  
  nodes.push(def_row)

  this.state.nodeRows=scanNodes(nodes,cells)
  this.forceUpdate()
}

function scanNodes(inputs,cells,key=""){
  var data=[]
  inputs.forEach(input=>{
    var row=[]
    cells.forEach((cell,i)=>{
      if(cell.key){
        var value=input[cell.key]
        if(cell.type==="dropdown" && value!==null){
          var option=cell.options.filter(opt=> opt.value===value)
          value=option[0].label
        }
        row.push(value)
      }
    })
    data.push(row)
  })
  return data
}

function handleNodeChange(changes,source,nodes,config){
  if(source==="Autofill.fill"){

  }
  else if(source!=="loadData"){
    const row=changes[0][0]
    const col=changes[0][1]
    var cell = config[col]
    var value = changes[0][3]

    if(cell["type"]==="dropdown" && value!==null){
      var option = cell.options.filter(opt=> opt.label===changes[0][3])
      if (option.length!==0)
        value = option[0].value
      
      //case of autofill node cells on selection of dropdown
      if("on_select" in cell){
        Object.assign(nodes[row], cell.on_select[value.toString()])
        parseInput()
        this.forceUpdate()
      }
    }
    var keys=cell["key"]
    if(keys!==undefined){
      keys=keys.split(".")
      if(keys.length===1)
        nodes[row][keys[0]]=value
    }


    console.log("changed Node")
  }
}

function getValue(input,keys){
  var key=keys.shift()
  return keys.length===0?input[key]:getValue(input[key],keys)
}

function addValue(obj,keys,value){
  var key = keys.shift()
  if(keys.length!==0){
    if(!obj.hasOwnProperty(key))
      obj[key]={}
    addValue(obj[key],keys,value)
  }else{
    obj[key]=value
  }
}

function updateTab(){
  this.forceUpdate()
}


export default App;
