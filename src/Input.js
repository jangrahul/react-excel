const Input=()=>{
	return([
		{
		  "session_id": "BSONId ",
		  "airtel_sr_no": "123 ",
		  "sr_type": 150,
		  "options": {
		    "is_noc": false,
		    "is_mw": false,
		    "is_fiber": false,
		    "is_2g": true,
		    "is_3g": true,
		    "is_4g_td": true,
		    "is_4g_fd": true,
		    "is_4g": "->{ self.is_4g_td || self.is_4g_fd }"
		  },
		  "error": {
		    "dynamic keys": "key specific error"
		  },
		  "existing_site_id": {
		    "tech_2g": "2g",
		    "tech_3g": "3g",
		    "tech_4g": null
		  },
		  "site_id": {
		    "tech_2g": "",
		    "tech_3g": "",
		    "tech_4g": "4g"
		  },
		  "common_data": {
		    "circle_id": "AS",
		    "infl_newtwn": "NEW TOWN",
		    "technology": "will this be entered by user or infered from options ???",
		    "site_name": "site_name",
		    "toco_site_id": "",
		    "toco_id": "an id",
		    "locator_id": "what is this ???",
		    "site_addr": "",
		    "city_id": 170,
		    "state_id": 5,
		    "pincode": "",
		    "tentative_lat": "",
		    "tentative_lon": "",
		    "sc_nsc": "SC",
		    "rfai_target_date": null,
		    "aggr_pwr_ld": "",
		    "tower_type": 450,
		    "tower_height": "",
		    "tower_mount_cnt": "",
		    "mcb": {
		      "rating_06a": "6a",
		      "rating_10a": "",
		      "rating_16a": "",
		      "rating_32a": "",
		      "rating_63a": "",
		      "rating_80a": "80a"
		    },
		    "other_nodes": [
		      {
		        "planner_type": "is this really required ???",
		        "name": 100,
		        "type": "",
		        "manufacturer": "",
		        "make": "",
		        "length": "",
		        "breadth": "",
		        "height": "",
		        "voltage": "",
		        "pwr_rtg": "",
		        "full_rack": "",
		        "tx_rack_space": "123",
		        "row_in_toco": "",
		        "fiber_laying_type": "",
		        "fms_type": ""
		      },
		      {
		        "planner_type": "is this really required ???",
		        "name": 150,
		        "type": "",
		        "manufacturer": "",
		        "make": "",
		        "length": "",
		        "breadth": "",
		        "height": "",
		        "voltage": "",
		        "pwr_rtg": "",
		        "full_rack": "",
		        "tx_rack_space": "",
		        "row_in_toco": "",
		        "fiber_laying_type": "",
		        "fms_type": ""
		      }
		    ],
		    "error": {
			    "dynamic keys": "key specific error"
			}
		  },
		  "toco_data": {
			    "site_id": "",
			    "name": ""
			  },
		  "rf_data": {
			    "band_freq": {
			      "tech_2g": "12",
			      "tech_3g": "45",
			      "tech_4g": ""
			    },
			    "num_carriers": {
			      "tech_2g": "",
			      "tech_3g": "67",
			      "tech_4g": "56"
			    },
			    "ring_radius": "what is this ???",
			    "cell_type": "GSM",
			    "site_type": 150,
			    "tower_type": 450,
			    "product_type": "what is this ??? which master ???",
			    "amsl": "what is this and should this be in rf or in common ???",
			    "plan_type": 150,
			    "halted_site": "what is this ???",
			    "indr_outdr": "indoor",
			    "crtcl_catgry": 150,
			    "num_sectors": {
			      "tech_2g": "",
			      "tech_3g": "",
			      "tech_4g_td": "",
			      "tech_4g_fd": ""
			    },
			    "customer_priority": [
			      {
			        "site_addr": "site_addr",
			        "tower_type": 150,
			        "latitude": "",
			        "longitude": ""
			      }
			    ],
			    "antenna_swap_req": false,
			    "antenna_swap_cnt": "45",
			    "antenna": [
			      {
			        "type": 150,
			        "band_freq": "900",
			        "ports": "",
			        "weight": "",
			        "height": "",
			        "length": "",
			        "width": "",
			        "gain": "",
			        "temp_senstvty": "",
			        "azimuth": "which master ???",
			        "rf_agl": "which master ???"
			      }
			    ],
			    "config": "",
			    "config_carrier": "",
			    "num_bts_bbu": "is this needed ???",
			    "bts_bbu": [
			      {
			        "tech": "2G",
			        "make": "BBU5216",
			        "type": "",
			        "vendor": "",
			        "length": "",
			        "width": "",
			        "height": "",
			        "voltage": "",
			        "power": "",
			        "weight": "",
			        "temp_sensitivity": "",
			        "technology": "",
			        "space_occupied_in_u": "",
			        "location_type": 100,
			        "rru_combined_wt": "",
			        "rru_space": "",
			        "rru_temp_sensitivity": ""
			      },
			      {
			        "tech": "3G",
			        "make": "BBU5216",
			        "type": "",
			        "vendor": "",
			        "length": "",
			        "width": "",
			        "height": "",
			        "voltage": "",
			        "power": "",
			        "weight": "",
			        "temp_sensitivity": "",
			        "technology": "",
			        "space_occupied_in_u": "",
			        "location_type": 100,
			        "rru_combined_wt": "",
			        "rru_space": "",
			        "rru_temp_sensitivity": ""
			      },
			      {
			        "tech": "4G (TD)",
			        "make": "BBU5216",
			        "type": "",
			        "vendor": "",
			        "length": "",
			        "width": "",
			        "height": "",
			        "voltage": "",
			        "power": "",
			        "weight": "",
			        "temp_sensitivity": "",
			        "technology": "",
			        "space_occupied_in_u": "",
			        "location_type": 100,
			        "rru_combined_wt": "",
			        "rru_space": "",
			        "rru_temp_sensitivity": ""
			      },
			      {
			        "tech": "4G (FD)",
			        "make": "BBU5216",
			        "type": "",
			        "vendor": "",
			        "length": "",
			        "width": "",
			        "height": "",
			        "voltage": "",
			        "power": "",
			        "weight": "",
			        "temp_sensitivity": "",
			        "technology": "",
			        "space_occupied_in_u": "",
			        "location_type": 100,
			        "rru_combined_wt": "",
			        "rru_space": "",
			        "rru_temp_sensitivity": ""
			      }
			      
			    ],
			    "rru": [
			          {
			          	"technology":"2G",
			            "rru_location": "",
			            "rru_model": "",
			            "rru_height": "",
			            "rfai_rru_space": "why is this needed separately ???"
			          }
			    ],
			    "tma_tmb_cnt": "is this needed or will the array length suffice ???",
			    "tma_tmb_combined_wt": "",
			    "tma_tmb": [
			      {
			        "weight": "123",
			        "height": "213"
			      }
			    ]
		  },
		  "mw_data": {
			    "antenna_swapped_req": "",
			    "antenna_remove_cnt": "",
			    "antenna_cnt": "is this needed or will array length suffice ???",
			    "make": "should this be per antenna or common ???",
			    "antenna": [
			      {
			        "type": "see MW_TYP_MAST.csv",
			        "diameter": "values for 'dia' to 'weight', see MW_ANTENNA_MAST.csv",
			        "freq": "",
			        "antenna_model": "",
			        "band": "",
			        "weight": "",
			        "high_low": "which master ???",
			        "pwr_ld": "which master ???",
			        "azimuth": "which master ???",
			        "agl": "which master ???",
			        "link_id": "which master ???"
			      }
			    ],
			    "pwr_plant_voltage": "",
			    "idu_cnt": "is this needed or will array length suffice ???",
			    "idu": [
			      {
			        "model": "for all values see MW_IDU_MAST.csv",
			        "power": "",
			        "temp_sensitivity": "",
			        "trnsmn_rack_space": ""
			      }
			    ]
		  },
		  "fiber_data": {
			    "provisioning_required": false,
			    "row_required": false,
			    "equipment_required": false,
			    "type_laying": 150,
			    "pop": 150,
			    "node_cnt": "is this needed or will array length suffice ???",
			    "node": [
			      {
			        "type": 200,
			        "name": "for values 'name' to 'tx_rack_space' see FIBER_MAST.csv",
			        "manufacturer": 200,
			        "make": "",
			        "length": "",
			        "breadth": "",
			        "height": "",
			        "volt": "",
			        "power_rating": "",
			        "rack_type": "",
			        "temp_sensitivity": "",
			        "tx_rack_space": "",
			        "fdms_req": false,
			        "fdms_type": 150
			      }
			    ]
			  }
		}
])
}

export default Input