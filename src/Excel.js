import React, { Component } from 'react'
import HotTable from 'react-handsontable'
import Handsontable from 'handsontable'
import Node from './Node'
import { renderReactCell, MemoizedReactDomContainers } from 'react-lru'
import clone from 'fast-clone'
import EllipsisText  from 'react-ellipsis-text'
import ReactTooltip from 'react-tooltip'

const memoizedContainers = new MemoizedReactDomContainers()

export default class Excel extends Component {
	constructor(props){
		super(props)
		this.removedRows=[]
		this.removedNodes=[]
		this.state={
			showNodes: [-1,""],
			copiedRows: []
		}
		nodeCellProperties=nodeCellProperties.bind(this)
		setShowNodes=setShowNodes.bind(this)
		handleNodeChange=handleNodeChange.bind(this)
	  	
	}
	render(){
	  var row, col, keys, nodeText, nodes, value;

	  if(this.props.showNodes[0]!==-1){
	      [row,col,keys]=this.props.showNodes
	      nodeText= keys.split('.').pop()
	      nodes= this.props.config[nodeText]
	      value= getValue(this.props.input[row],keys.split("."))
	    }
		return(
			<div>
			<HotTable root="main" ref="main" settings={{
	          data: this.props.values,
	          rowHeaders: function(index) {
	            if(index===0||index===1){
	              return ""
	            }
	            return index-1
	          },
	          width: "1270",
	          height: "300",
	          stretchH: "all",
	          mergeCells: this.props.mergeCells,
	          manualColumnResize: true,
	          fixedColumnsLeft: 2,
	          fixedRowsTop: 2,
	          onAfterChange: handleChange.bind(this),
	          cells: cellProperties.bind(this),
	          contextMenu: {"remove_row": {disabled: 
	          	function(){
	          	return this.refs.main.hotInstance.getSelected()[0]<2} },
	          },
	          contextMenu: ["remove_row"],
	          onAfterRemoveRow: handleRemoveRow.bind(this),
	          onAfterCreateRow: handleCreateRow.bind(this),
	          onBeforeRemoveRow: handleBeforeRemoveRow,
	          onBeforeCreateRow: handleBeforeCreateRow,
	          onBeforeCopy: beforeCopy.bind(this),
	          onBeforePaste: beforePaste.bind(this),
	          onAfterPaste: afterPaste.bind(this),
	          onAfterBeginEditing: afterBeginEditing.bind(this),
	          onAfterDocumentKeyDown: afterkeyDown.bind(this)
	          }}  />
	        <form onSubmit={this.props.addRows}>
	            <input type="number" name="count" defaultValue="1"></input>
	            <button type="submit">Add Row(s)</button>
	           </form>
	        {
	          (this.props.showNodes[0]!==-1)&&
	          <Node 
	            text={nodeText}
	            closePopup={()=>(this.props.setShowNodes(-1))}
	            addRow={()=>(this.props.addNodeRow(this.props.showNodes[0],keys))}
	            headers={this.props.nodeHeaders}
	            data={this.props.nodeRows}
	            cellProperties={(row,col,prop)=>(nodeCellProperties(row,col,prop,nodes))}
	            onAfterChange={(changes,source)=>(handleNodeChange(changes,source,value,nodes))}
	            onAfterRemoveNode= {handleRemoveNode.bind(this)}
	          	onAfterCreateNode= {handleCreateNode.bind(this)}
	          	onBeforeCreateNode={handleBeforeCreateNode.bind(this)}
	          />
        	}
        	</div>
			);
	}
}

class NodeCell extends React.Component {
    render() {
        return (<div>
        		<button type="button" 
            		className="node_button"
            		onClick={()=>{this.props.showOtherNodes(this.props.row, this.props.col, this.props.values)}}>
            			<EllipsisText text={this.props.nodeText} length={'20'} />
            	</button>
        </div>);
    }
}

function cellProperties(row,col,prop){
  const cells = this.props.cells
  var cellProperties = {};

              if (row===0||row===1) {
                cellProperties.readOnly = true;
                cellProperties.disableVisualSelection= true;
                cellProperties.renderer = firstRowRenderer;
              }
              if (row>1){
                let cell = cells[col]
                
                if(cell.type === "dropdown"){
                  cellProperties.source= getSource(cell.options,this.refs,row,col)
                }
                 
                 if(Array.isArray(cell.default)){
                  cellProperties.renderer=nodeRenderer.bind(this);
                 } 
                 else{
                 	cellProperties.type= cell.type;
                 	cellProperties.default=cell.default;
                 }

                 //conditions for enabling/disabling technology columns
                 if(cell.key.split('.').pop()==="tech_2g"){
                  cellProperties.readOnly=!(this.props.rows[row-2][2].toString()==="true")
                 }
                 else if(cell.key.split('.').pop()==="tech_3g"){
                  cellProperties.readOnly=!(this.props.rows[row-2][3].toString()==="true")
                 }
                 else if(cell.key.split('.').pop()==="tech_4g"){
                  cellProperties.readOnly=!(this.props.rows[row-2][4].toString()==="true"||this.props.rows[row-2][5].toString()==="true")
                 }
                 else{
                 	cellProperties.readOnly=!cell.enabled; 
                 }
              }
              return cellProperties;

              
}

function nodeCellProperties(row,col,prop,nodes){
  var cellProperties={}
  var node=nodes[col]
  cellProperties.readOnly=!node.enabled
  cellProperties.type=node.type
    if(node.type==="dropdown"){
      cellProperties.source=getSource(node.options,this.refs)
    }
  return cellProperties
}


//relects the changed node values in original JSON
function handleNodeChange(changes,source,nodes,config){
  if(source==="Autofill.fill"){

  }
  else if(source!=="loadData"){
    const row=changes[0][0]
    const col=changes[0][1]
    var cell = config[col]
    var value = changes[0][3]

    if(cell["type"]==="dropdown" && value!==null){
      var option = cell.options.filter(opt=> opt.label===changes[0][3])
      if (option.length!==0)
        value = option[0].value
      
      //case of autofill node cells on selection of dropdown
      if("on_select" in cell){
      	Object.assign(nodes[row], cell.on_select[value.toString()])
    	let values=[]
    	let table_row=this.props.showNodes[0]
    	let table_col=this.props.showNodes[1]
    	if(this.props.input[table_row]){
      		values=getValue(this.props.input[table_row],(this.props.cells[table_col].key).split('.'))
    	}
    	this.props.showOtherNodes(table_row+2,table_col,values)
      }
    }
    var keys=cell["key"]
    if(keys!==undefined){
      keys=keys.split(".")
      if(keys.length===1)
        nodes[row][keys[0]]=value
    }
    console.log("changed Node")
  }
}

function nodeRenderer(instance, td, row, col, prop, value) {
  
    let values = []
    let nodeText=""
    if(this.props.input[row-2]){
      var keys=[]
	  let options=[]
	  keys=(this.props.cells[col].key).split('.')
      values=getValue(this.props.input[row-2], Object.assign([],keys))
      let nodeCells=this.props.config[keys[keys.length-1]]

      if (nodeCells[0].type==="dropdown"){
      	options=nodeCells[0].options
      }

      nodeText=getNodeText(values,options)
    }
    renderReactCell({
        memoizedContainers,
        td,
        row,
        col,
        jsx: <NodeCell 
        		row={row} 
        		col={col} 
        		values={values}
        		nodeText={nodeText}
        		showOtherNodes={this.props.showOtherNodes}
        		/>,
    });

    return td;
}

function getNodeText(values,options){
	let nodeText=""
	let first_key = ""
      
  	if(values[0]!==undefined){
  		first_key=Object.keys(values[0])[0]

  		if(options.length===0){
  		nodeText = values.map(value=>{return value[first_key]}).join("; ")
	  	}
		else{
			//for dropdown column in node
			let v=values.map(value=>{let found=options.find(opt=>opt.value===value[first_key])
								if(found)
								return found.label }) ||""
			nodeText=v.join("; ")
		}
  	}
	return nodeText
}


function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.textAlign = 'center';
    td.style.fontWeight = 'normal';
    td.style.color = '#000';
    td.style.background = '#f3f3f3';
    td.style.whiteSpace = "nowrap";
  }

function getValue(input,keys){
  var key=keys.shift()
  return keys.length===0?input[key]:getValue(input[key],keys)
}

function getSource(sources,refs,row=0,col=0){
  let values = []
  if(Object.keys(refs).length!==0 && refs.main.hotInstance.getDataAtCell(1,col)==="City"){
    
    const state=refs.main.hotInstance.getDataAtCell(row,col-1)
    sources.forEach(obj=>{
      if(obj.state === state){
        values.push(obj.label)
      }
    })      
  }
  else{
    sources.forEach(obj=>{
      values.push(obj.label)
    })
  }
  return values
}

//reflects the changes in original JSON
function handleChange(changes,source){

    if(source!=="loadData"){
  	changes.forEach(change=>{
		const row=change[0]-2
    const col=change[1]
    const config=this.props.config
    const cells = this.props.cells
    var cell = cells[col]
    var value = change[3]

    if(cell["type"]==="dropdown" && value!==null){
      var option = cell.options.filter(opt=> opt.label===change[3])
      if (option.length!==0)
        value = option[0].value
    }
    var keys=cell["key"]
    if(keys!==undefined){
      keys=keys.split(".")
      addToInput(this.props.input[row],keys,value)
    }

    //cases when technology fields are toggled
    if(col>1 && col<6 ){
    	var rf_data=this.props.input[row].rf_data
    	if(col===2){
    		value.toString()==="true" ? addToNode(rf_data,"2G"):removeFromNode(rf_data,"2G")
    	}
    	if(col===3){
    		value.toString()==="true" ? addToNode(rf_data,"3G"):removeFromNode(rf_data,"3G")
    	}
    	if(col===4){
    		value.toString()==="true" ? addToNode(rf_data,"4G (TD)"):removeFromNode(rf_data,"4G (TD)")
    	}
    	if(col===5){
    		value.toString()==="true" ? addToNode(rf_data,"4G (FD)"):removeFromNode(rf_data,"4G (FD)")
    	}
    	this.props.parseInput()
    	this.props.updateTab()
    }
    console.log("changed")
  	})
  }
}

//add to bts_bbu node
export function addToNode(rf_data, tech){
	var nodes=rf_data.bts_bbu
	if(rf_data.removed_nodes!==undefined){
		var n=rf_data.removed_nodes.find(node=>node.tech===tech)
		if(n){
			nodes.push(n)
		}
	}
}

//remove from bts_bbu node
export function removeFromNode(rf_data, tech){
	const nodes=rf_data.bts_bbu
	var n=nodes.filter(node=>node.tech===tech)
	if(n.length!==0){
		var index=nodes.indexOf(n.pop())
		if(rf_data.removed_nodes===undefined)
			rf_data.removed_nodes=[]
		rf_data.removed_nodes.push(nodes.splice(index,1)[0])
	}	 
}

function handleRemoveRow(index,amount){
	this.removedRows=this.removedRows.concat(this.props.input.splice(index-2,amount))
	this.props.parseInput()
}

function handleCreateRow(index, amount, source){
	if(source==="UndoRedo.undo"){
		if(index<2)
			return 
		for(var i=0;i<amount;i++){
			this.props.input.splice(index-2,0,this.removedRows.pop())
		}
		this.props.parseInput()
	}
	else if(source==="Autofill.fill"){
		this.props.addRows("dummyEvent",amount)
	}
}

function handleRemoveNode(index,amount){
	this.removedNodes = this.removedNodes.concat(this.props.nodeInput.splice(index,amount))
}

function handleCreateNode(index, amount, source){
	if(source==="UndoRedo.undo"){
		for(var i=0;i<amount;i++){
			this.props.nodeInput.splice(index,0,this.removedNodes.pop())
		}
	
	}
	else if(source==="Autofill.fill"){
		return false
	}
}

function handleBeforeRemoveRow(index,amount,visualRows){
	if(index<2)
		return false
}

function handleBeforeCreateRow(index,amount,source){
	if(source==="UndoRedo.undo"){
		if(index<2)
			return false
	}else if(source==="Autofill.fill"){
		return false
	}
}

function handleBeforeCreateNode(index,amount,source){
	if(source==="Autofill.fill"){
		return 
	}
}

function addToInput(input,keys,value){
  let key=keys.shift()
  if(keys.length===0){
    input[key]=value
    return 
  }
  input[key]=input[key]||{} 
  addToInput(input[key],keys,value)
}

function beforeCopy(data, coords){
	this.state.copiedRows=[]
	var startRow=coords[0].startRow-2
	var startCol=coords[0].startCol
	var endRow=coords[0].endRow-2
	var endCol=coords[0].endCol
	if(startCol===0 && endCol===71){
		for(var i=startRow; i<=endRow; i++){
			// var row= JSON.parse(JSON.stringify(this.props.input[i])) //Deep copy
			var row = clone(this.props.input[i])
			this.state.copiedRows.push(row)
		}
	}
}

function beforePaste(data,coords){
	var startRow=coords[0].startRow-2
	var startCol=coords[0].startCol
	var endRow=coords[0].endRow-2
	var endCol=coords[0].endCol
	var len=this.state.copiedRows.length

	if(startCol===0 && endCol===71){
		for(var i=startRow;i<=endRow;i+=len){
			for(var j=0;j<len;j++){
				// var row= JSON.parse(JSON.stringify(this.state.copiedRows[j])) //deep copy
				var row = clone(this.state.copiedRows[j])
				this.props.input[i]=row
				checkBtsNodes.bind(this)(i)
			}
		}
	this.props.parseInput()
	this.props.updateTab()
	}
}

function afterPaste(data,coords){
	var startRow=coords[0].startRow-2
	var startCol=coords[0].startCol
	var endRow=coords[0].endRow-2
	var endCol=coords[0].endCol
	var len=this.state.copiedRows.length

	this.props.parseInput()
	this.props.updateTab()

}

function checkBtsNodes(row){
  var rf_data=this.props.input[row].rf_data
  var input=this.props.input[row]
        
        input.options.is_2g.toString()==="true" ? addToNode(rf_data,"2G"):removeFromNode(rf_data,"2G")
        
        input.options.is_3g.toString()==="true" ? addToNode(rf_data,"3G"):removeFromNode(rf_data,"3G")
        
        input.options.is_4g_td.toString()==="true" ? addToNode(rf_data,"4G (TD)"):removeFromNode(rf_data,"4G (TD)")
        
        input.options.is_4g_fd.toString()==="true" ? addToNode(rf_data,"4G (FD)"):removeFromNode(rf_data,"4G (FD)")
}

function afterBeginEditing(row,col){
	let values = []
	const instance=this.refs.main.hotInstance
	let cell_name=instance.getCellRenderer(row,col).name
	if(cell_name==="bound nodeRenderer"){
		if(this.props.input[row-2]){
      		values=getValue(this.props.input[row-2],(this.props.cells[col].key).split('.'))
    	}
    	instance.deselectCell()
		this.props.showOtherNodes(row,col,values)
		return false
	}
	
	console.log('node_clicked')
}

function setShowNodes(value,key){
  this.setState({
    showNodes: [value,key]
  })
}

function afterkeyDown(event){
	let c=0
	if(event.keyCode===27 && this.props.showNodes[0]!==-1){
		this.props.setShowNodes(-1)
	}
}
